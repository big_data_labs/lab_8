#!/bin/sh

set -e
sudo kubeadm init --control-plane-endpoint=k8s-lab --pod-network-cidr=10.244.0.0/16