# K8S

## Стркутура проекта

* `mcdnmd/lab_4:7aa10c76`- FastApi backend для обработки запросов к ML алгоритму
* `mongo:latest` - База данных для хранения промежуточны и итоговых данных
* `mcdnmd/lab_7:46cab28a` - Spark приложение для обработки данных


FastApi backend будет запущен как сервис, чтобы можно было достучаться из сети до него. 
Mongo деплоится как сервис и имеет presistent volume для хранения данных.
Для упрощения маршрутизации по хосту будет использован Ingress нода

--- 

## Полезные команды k8s
1. Получение неймспейсов в кластере
   ```shell
   kubectl get namespace
   ```

2. Полуение всех подов в неймспейсе
   ```shell
   kubectl -n mcdnmd get pods
   ```

3. Получение ивентов в неймспейсе
   ```shell
    kubectl -n mcdnmd get events
   ```

4. Удаление деплоймента в неймспейсе
   ```shell
   kubectl -n mcdnmd delete deployment <deploy name>
   ```

---

## Minikube

1. Запустить minikube
   ```shell
   minikube start
   ```

2. Остановить minikube
   ```shell
   minimube stop
   ```

3. Прокинуть тунель до Service в k8s
   ```shell
   minikube tunnel
   ```

4. Добавить Ingress для маршрутизации по хосту 127.0.0.1
   ```shell
   minikube addons enable ingress
   ```

---

## Пайплайн создания проекта в k8s

1. Создание нейспейса **mcdnmd**
    ```shell
    kubectl create -f ./k8s/namespace.yaml
    ```

2. Создание persistent volume
   ```shell
   kubectl -n mcdnmd apply -f ./k8s/persistent-volume.yaml   
   ```   

3. Создание поды базы данных mongo 
   ```shell
   kubectl -n mcdnmd apply -f ./k8s/mongo/deployment.yaml
   kubectl -n mcdnmd apply -f ./k8s/mongo/service.yaml
   ```

4. Создание в неймспейсе поды бекенда
    ```shell
    kubectl -n mcdnmd apply -f ./k8s/api/deployment.yaml
    kubectl -n mcdnmd apply -f ./k8s/api/service.yaml
    ```

5. Создание в неймспейсе поды spark
    ```shell
    kubectl -n mcdnmd apply -f ./k8s/spark/application.yaml
    ```

6. Создание ингреса в неймспейсе
   ```shell
   kubectl -n mcdnmd apply -f ./k8s/ingress.yaml
   ```